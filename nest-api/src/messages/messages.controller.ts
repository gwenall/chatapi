import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { MessagesService } from './messages.service';
import { CreateMessageDto } from './dto/create-message.dto';
import { UpdateMessageDto } from './dto/update-message.dto';
import { ApiBody, ApiOperation, ApiProperty, ApiQuery, ApiTags } from '@nestjs/swagger';
import {PaginationQuery} from "../commons/interfaces"

const apiDoc = {
  limit : {name:"limit", description:"max number of messages being returned", required:false, type:"number"},
  offset: {name:"offset", description:"skip the first nth results. Defaults to zero if not specified, ignored if no limit is set", required:false, type:"number"}
};

@ApiTags("messages")
@Controller('messages')
export class MessagesController {
  constructor(private readonly messagesService: MessagesService) {}

  @Post()
  @ApiOperation({summary:"create one message. Returns the full message object"})
  create(@Body() createMessageDto: CreateMessageDto) {
    return this.messagesService.create(createMessageDto);
  }

  @ApiQuery(apiDoc.limit)
  @ApiQuery(apiDoc.offset)
  @ApiOperation({summary:"returns every messages ever written (for debug purposes)"})
  @Get()
  findAll(@Query() paginationQuery : PaginationQuery|undefined) {
    return this.messagesService.findAll(paginationQuery);
  }

  @Get('/byID/:id')
  @ApiOperation({summary:"returns one message corresponding to this message.id (or error 404 if not found)"})
  findOne(@Param('id') id: string) {
    return this.messagesService.findOne(id);
  }

  @ApiQuery(apiDoc.limit)
  @ApiQuery(apiDoc.offset)
  @Get('byAuthorId/:authorId')
  @ApiOperation({summary:"returns messages sent by one user in particular, identified by it's user.id"})
  findByAuthorId(@Param('authorId') authorId: string, @Query() paginationQuery : PaginationQuery|undefined) {
    return this.messagesService.findByAuthorId(authorId, paginationQuery);
  }

  @ApiQuery(apiDoc.limit)
  @ApiQuery(apiDoc.offset)
  @ApiOperation({summary:"returns message sent to one or more recipient(s) identified by their user.id",
                 description: "takes an array of recipients ID (strings) to which the messages to retrieve are addressed and return the messages sorted by date (newer first)"})
  @Get('byRecipientsIDs')
  findByRecipientsIds (@Body() recipientsIDs: string[], @Query() paginationQuery : PaginationQuery|undefined) {
    return this.messagesService.findByRecipientsIds(recipientsIDs, paginationQuery);
  }

  @Patch(':id')
  @ApiOperation({summary:`Updates one or more attribute of a message object. The message should have been previously created by the POST route.`,
                description: `takes an object containing one or more attribute of the Message class and return the full updated message object`})
  update(@Param('id') id: string, @Body() updateMessageDto: UpdateMessageDto) {
    return this.messagesService.update(id, updateMessageDto);
  }
  
  @Delete(':id')
  @ApiOperation({summary:"delete one message identified by it's message.id"})
  remove(@Param('id') id: string) {
    return this.messagesService.remove(id);
  }
}
