import { Entity, Column, PrimaryGeneratedColumn, JoinColumn, OneToMany, ManyToOne, ManyToMany, JoinTable, CreateDateColumn } from "typeorm";
import { User } from "src/users/entities/user.entity";
import { ApiProperty } from "@nestjs/swagger";

@Entity()
export class Message {

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @ApiProperty({
        description: "the body of the message",
        example: "hi, how is your project going ?"
    })
    @Column()
    text: string;

    @ApiProperty({
        description: "the author's ID",
        example: "5ac4af0d-7603-4be9-9e3a-c8f7e9290e25"
    })

    // CASCADE will remove the message if the author user gets deleted
    @ManyToOne (() => User, user => user.writtenMessages, {onDelete: "SET NULL", onUpdate:"NO ACTION"})
    author:User;

    @ManyToMany(() => User, user => user.id)
    @JoinTable()
    @ApiProperty({
        description: "a list of recipients (type User) stored on a many-to-many hash table",
    })
    recipients:User[];

    @CreateDateColumn()
    createdAt:Date;


    // @ApiProperty({
    //     description: "a list of recipients ids which have seen this message in JSON.strigify format, defaults to null",
    //     example: "[\"5ac4af0d-7603-4be9-9e3a-c8f7e9290e25\",\"5ac4af0d-7603-4be9-9e3a-c8f7e9290e25\"]"
    // })
    // @Column("simple-json", {nullable: true})
    // seenBy: string[];

}
