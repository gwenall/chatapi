import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { Repository, EntityNotFoundError, Equal, In } from 'typeorm';
import { CreateMessageDto } from './dto/create-message.dto';
import { UpdateMessageDto } from './dto/update-message.dto';
import { Message } from './entities/message.entity';
import { PaginationQuery } from 'src/commons/interfaces';

@Injectable()
export class MessagesService {
  constructor(

    @InjectRepository(Message)
    private readonly messageRepo : Repository<Message>,

    @InjectRepository(User)
    private readonly userRepo : Repository<User>
  ){}

  async create(createMessageDto: CreateMessageDto) {
    const recipients: User[] = await Promise.all(
      createMessageDto.recipientsIDs.map( async (recipientID: string): Promise<User> => {
        const user: User|void = await this.preloadUser(recipientID);
        if (user) return user;
        else throw new NotFoundException(`recipient #${recipientID} wasn't found in users`);
    })
    );
    const message: Message = this.messageRepo.create({...createMessageDto, recipients});
    return this.messageRepo.save(message)
  }

  findAll(paginationQuery: PaginationQuery|void = null) {
    // this method will return the whole author object without being able to filter it
    
    // const messages = this.messageRepo.find({
    //   relations:["author"],
    // });
    // return messages.map(m=> ({...m, author:{id: m.author.id, name: m.author.name}}))
    
    // So a basic query it is
    const queryResults = this.getQueryBuilder(paginationQuery).orderBy("message.createdAt", "DESC");
      return queryResults.getMany()
  }

  async findOne(id: string) {
    return this.getQueryBuilder().where("message.id = :id", {id}).getOneOrFail()
    .catch(error => {
      if (error instanceof EntityNotFoundError)
        throw new NotFoundException(`unable to found message with id ${id}`);
      else throw(error);
    });
  }

  async update(id: string, updateMessageDto: UpdateMessageDto) {
    // if ("recipientsID" in updateMessageDto) updateMessageDto.recipients = this.preloadUser(UpdateMessageDto.)
    const message = await this.messageRepo.preload({
      id, ...updateMessageDto
    });
    if (!message) throw new NotFoundException(`could not update message ${id} : message wasn't found`);
    return this.messageRepo.save(message)
  }

  async remove(id: string) {
    const message = await this.findOne(id);
    return this.messageRepo.remove(message);
  }

  async findByAuthorId(userid: string, paginationQuery:PaginationQuery|void = null): Promise<Message[]> {
    const messages: Message[] = await this.getQueryBuilder(paginationQuery)
    .where("author.id = :userid", {userid})
    .orderBy("message.createdAt", "ASC")
    .getMany();
    return messages;
  }

  async findByRecipientsIds(recipientsIDs: string[], paginationQuery:PaginationQuery|void = null): Promise<Message[]> {
    console.log("recipientsIDS : ", recipientsIDs, recipientsIDs.length);

    const paginationOptions = paginationQuery && "limit" in paginationQuery 
    ? {take:paginationQuery.limit, skip:paginationQuery.offset || 0}
    : {}

    let messages: Message[] = await this.messageRepo.find({
      relations: ["recipients"],
      order: { createdAt: "DESC"},
      ...paginationOptions
    })
    // filter messages where at least one user has its ID in the recipientsIDs array
    messages = messages.filter(m=>m.recipients.some(u=>recipientsIDs.includes(u.id)))
    return messages;
  }

  // returns a query builder with the author relation
  // allowing to select properties out of the User entity
  private getQueryBuilder (paginationQuery: PaginationQuery|void = null) {
    let qb = this.messageRepo.createQueryBuilder("message")
    .leftJoinAndSelect("message.author", "author")
    .select(["message","author.id", "author.name"])

    if (paginationQuery && "limit" in paginationQuery) {
      paginationQuery.offset = paginationQuery.offset || 0;
      qb = qb.take(paginationQuery.limit).skip(paginationQuery.offset);
    }
    return qb
  }

  private preloadUser = (id: string) : Promise<User | void> => {
    const user = this.userRepo.findOneOrFail({id})
    .catch(
      e => {
        console.error(`error while preloading user #${id}`, e)
      }
    );
    return user;
  }
}
