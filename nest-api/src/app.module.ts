import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { MessagesModule } from './messages/messages.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type:"mysql",
      host:"mysql",
      port:3306,
      username:"root",
      password:process.env.MYSQL_ROOT_PASSWORD,
      database:process.env.MYSQL_DATABASE,
      autoLoadEntities: true,
      synchronize:true,
    }),
    UsersModule,
    MessagesModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
