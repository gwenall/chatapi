import { IsDate, IsEmail, IsOptional, IsString, IsUUID } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class CreateUserDto {
    
    @ApiProperty({description: "name or surname of this user"})
    @IsString()
    name: string;
    
    @ApiProperty({description: "this email will be validated"})
    @IsEmail()
    email: string;
    
    @ApiProperty({description: "optionnal, can be anything you like ('on line', 'do not disturb'...)", required:false})
    @IsString()
    @IsOptional()
    status: string;

}
