import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Equal, Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
  ){}

  async create(createUserDto: CreateUserDto) {
    const user = this.usersRepository.create(createUserDto);
    return this.usersRepository.save(user);
  }

  async findAll() {
    return await this.usersRepository.find();
  }

  async findOne(id: string) {
    const user = await this.usersRepository.findOneOrFail({id})
    .catch(e=>{
      throw new NotFoundException(`the user with id ${id} wasn't found in the database`);
    });
    return user;
  }

  // async findReceivedMessages(id: string) {
  //   const user = await this.getUser(id, {relations:["messagesReceived"]})
  //   return user.messagesReceived
  // }

  async findSentMessages(id: string) {
    const user = await this.getUser(id, {relations:["writtenMessages"]})
    return user.writtenMessages
  }
  
  async update(id: string, updateUserDto: UpdateUserDto) {
    const user = await this.usersRepository.preload(
      {id, ...updateUserDto}
      );
    if (!user) throw new NotFoundException(`the user with id ${id} wasn't found in the database`);
    return this.usersRepository.save(user);
  }

  async remove(id: string) {
    const user = await this.findOne(id);
    return this.usersRepository.remove(user);
  }

  async getUser(id: string, options: object = {}): Promise<User> {
    const user = await this.usersRepository.findOneOrFail({id, ...options})
    return user;
  }
}
