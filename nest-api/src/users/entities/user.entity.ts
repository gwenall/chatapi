import { ApiProperty } from "@nestjs/swagger";
import { Message } from "src/messages/entities/message.entity";
import {Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToMany, JoinTable, CreateDateColumn, JoinColumn} from "typeorm";

@Entity()
export class User {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @ApiProperty({
        description: "name or surname of this user. Does not need to be unique",
        example: "John Doe"
    })
    @Column()
    name: string;

    @ApiProperty({
        description: "this email will be validated",
        example:"john.doe@mail.com"
    })
    @Column()
    email:string;

    @Column({default: `Hi ! New user here`})
    status:string;

    @OneToMany(() => Message, message => message.author)
    writtenMessages: Message[];

    @ManyToMany(()=> Message, message => message.recipients)
    messagesReceived: Message[];

    @CreateDateColumn()
    createdAt:Date;
}
