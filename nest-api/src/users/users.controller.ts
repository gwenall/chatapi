import { Controller, Get, Post, Body, Patch, Param, Delete, Redirect } from '@nestjs/common';
import { ApiTags, ApiOperation } from "@nestjs/swagger";
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@ApiTags("users")
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  @ApiOperation({summary:"create one user. Returns the full user object"})
  create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  @Get()
  @ApiOperation({summary:"returns every user ever created (for debug purposes)"})
  findAll() {
    return this.usersService.findAll();
  }

  @Get(':id')
  @ApiOperation({summary:"returns one user corresponding to the provided id (or error 404 if not found)"})
  findOne(@Param('id') id: string) {
    return this.usersService.findOne(id);
  }

  // @Get(':id/receivedMessages')
  // @ApiOperation({summary:"returns the messages sent TO this user"})
  // findReceivedMessages(@Param('id') id: string) {
  //   return this.usersService.findReceivedMessages(id);
  // }

  @Get(':id/sentMessages')
  @ApiOperation({summary:"returns the messages sent BY this user"})
  findSentMessages(@Param('id') id: string) {
    return this.usersService.findSentMessages(id);
  }

  @Patch(':id')
  @ApiOperation({summary:"update one or more attribute of the user. Returns the full updated user object"})
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update(id, updateUserDto);
  }

  @Delete(':id')
  @ApiOperation({summary:"delete the user corresponding to the provided id"})
  remove(@Param('id') id: string) {
    return this.usersService.remove(id);
  }
}
