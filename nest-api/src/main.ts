import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {logger:["error", "warn", "debug", "log", "verbose"]});

  const config = new DocumentBuilder()
  .setTitle("Chat API for learning purposes")
  .setDescription("users and messages, that's all")
  .setVersion("0.0.1")
  .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup("", app, document);

  await app.listen(3000);
}
bootstrap();
